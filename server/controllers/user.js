const User = require("../models/user");
const Order = require("../models/order");
const bcrypt = require("bcrypt")
const auth = require("../auth")


module.exports.register = (body) => {

	let newUser = new User({
		firstName: body.firstName,
		lastName: body.lastName,
		email: body.email,
		password: bcrypt.hashSync(body.password, 10),
		mobileNo: body.mobileNo
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})

}

module.exports.login = (body) => {
	
	return User.findOne({email: body.email}).then(result => {
		if(result === null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password)
			if(isPasswordCorrect){
				return{accessToken: auth.createAccessToken(result.toObject())}
			}else{
				return false;
			}
		}
	})
}

module.exports.getOrders = (userId) => {
	return Order.find({userId:userId}).then(result => {
		return result
	})
}

module.exports.checkout = (body) => {

	let order = new Order({
		userId: body.userId,
		products: body.products,
		totalAmount: body.totalAmount
	})

	return order.save().then((order, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})

}

module.exports.getAllOrders = () => {
	return Order.find({}).then(result => {
		return result;
	})
}


