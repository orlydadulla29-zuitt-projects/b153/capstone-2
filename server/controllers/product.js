const Product = require("../models/product")

module.exports.addProduct = (body) => {
	let newProduct = new Product({
		name: body.name,
		description: body.description,
		price: body.price
	})

	return newProduct.save().then((product, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})	
}

module.exports.getProducts = () => {
	return Product.find({isActive:true}).then(result => {
		return result;
	})
}

module.exports.getSpecific = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (productId, body) => {
	let updatedProduct = {
		name: body.name,
		description: body.description,
		price: body.price
	}

	return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
		if(error){
			return false;
		}else{	
			return true;
		}
	})
}

module.exports.archiveProduct = (productId) => {
	let archivedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(productId, archivedProduct).then((product, error) => {
		if(error){
			return false;
		}else{	
			return true;
		}
	})
}