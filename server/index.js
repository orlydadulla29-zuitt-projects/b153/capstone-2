const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")

mongoose.connect("mongodb+srv://admin:admin@testdatabase1.8ulus.mongodb.net/computer_retail?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."))

const app = express()

app.use(express.json())
app.use(express.urlencoded({
	extended: true
}))

app.use("/users", userRoutes)
app.use("/products", productRoutes)


const port = 4000

app.listen(process.env.PORT || port, () => {
	console.log(`Server running on port ${port}`)
})