const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth")


router.post("/register", (req, res) => {
	console.log(req.body)
	userController.register(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.login(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/:userId/checkout", (req, res) => {
	console.log(req.body)
	userController.checkout(req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/:userId/myOrders", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id
	userController.getOrders(userId).then(resultFromController => res.send(resultFromController))
})

router.get("/:userId", (req, res) => {
	let currentUser = auth.decode(req.headers.authorization).isAdmin
	if(currentUser){
	userController.getAllOrders().then(resultFromController => res.send(resultFromController))
	}else{
		return res.send("auth: failed")
	}
})

module.exports = router;