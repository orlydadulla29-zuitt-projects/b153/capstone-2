const express = require("express")
const router = express.Router()
const productController = require("../controllers/product");
const auth = require("../auth");

router.post("/", auth.verify,(req, res) => {
	let currentUser = auth.decode(req.headers.authorization).isAdmin
	
	if(currentUser){
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send("auth: failed")
	}
})

router.get("/", (req, res) => {
	productController.getProducts().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req, res) => {
	productController.getSpecific(req.params.productId).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId", auth.verify, (req, res) => {
	let currentUser = auth.decode(req.headers.authorization).isAdmin

	if(currentUser){
	productController.updateProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send("auth: failed")
	}
})

router.delete("/:productId", auth.verify, (req, res) => {	
	let currentUser = auth.decode(req.headers.authorization).isAdmin

	if(currentUser){
	productController.archiveProduct(req.params.productId, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send("auth: failed")
	}
})

module.exports = router;